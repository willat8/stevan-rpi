import os
import sys
import pygame
import time
import subprocess
import random
import glob
from PIL import Image, ExifTags

### CONFIG STARTS HERE ###
PHOTO_PATH = '/home/pi/Pictures/'
PHOTO_FILES = ['*.jpg']
PHOTO_TIME = 10 # Time to show each photo in seconds

VIDEO_PATH = '/home/pi/Videos/'
VIDEO_FILES = ['*.f4v', '*.mov']
VIDEO_PROBABILITY = 0.0 # Probabability that a video will appear after a photo transition (0.1 is 10%)
### CONFIG ENDS HERE ###

FPS = 60

def main():
    # Assume we're using fbcon on Raspberry Pi
    os.putenv('SDL_VIDEODRIVER', 'fbcon')

    pygame.init()
    player = mediaplayer()
    clock = pygame.time.Clock()

    # Flat list of all files matching PHOTO_PATH and case-insensitive PHOTO_FILES
    photos = sorted(path for paths in ( insensitive_extension_glob(PHOTO_PATH, wildcard) for wildcard in PHOTO_FILES ) for path in paths)

    counter = -1 # Needs to start negative to trigger rising edge condition below at program start
    paused = False
    photos_length = len(photos)

    if (photos_length < 1):
        sys.exit()

    while True:
        tick = clock.tick(FPS)

        for event in pygame.event.get():
            if event.type == pygame.MOUSEBUTTONDOWN:
                paused = not paused # Toggle paused with every mouse down
                break # Primitive debounce? Only accept one mouse down per tick

        counter += 0 if paused else tick

        # Detect rising edge of a PHOTO_TIME interval
        if counter//1000 % PHOTO_TIME < (counter - tick)//1000 % PHOTO_TIME:
            # Show the next image
            player.show_image(photos[counter//(PHOTO_TIME*1000) % photos_length])

def insensitive_extension_glob(basepath, extension):
    def either(c):
        return '[%s%s]' % (c.lower(), c.upper()) if c.isalpha() else c
    return glob.glob(os.path.join(basepath, ''.join(map(either, extension))))

class mediaplayer:
    screen = None;
    screen_height = 0;
    screen_width = 0;
    
    def __init__(self):
        self.screen_height = pygame.display.Info().current_h
        self.screen_width = pygame.display.Info().current_w
        
        self.screen = pygame.display.set_mode([self.screen_width, self.screen_height], pygame.FULLSCREEN)
        self.screen.fill([0, 0, 0])        

        # This hides the mouse pointer which is unwanted in this application
        pygame.mouse.set_visible(False)
        pygame.display.update()

    def __del__(self):
        pass

    def show_image(self, filename):
        (imgbuffer, orientation) = self.open_image(filename)
        img = pygame.transform.rotate(pygame.image.frombuffer(*imgbuffer), orientation)

        img_height = img.get_height()
        img_width = img.get_width()

        # If the image isn't already the same size as the screen, it needs to be scaled
        if ((img_height != self.screen_height) or (img_width != self.screen_width)):
            # Determine what the height will be if we expand the image to fill the whole width
            scaled_height = int((float(self.screen_width) / img_width) * img_height)

            # If the scaled image is going to be taller than the screen, then limit the maximum height and scale the width instead
            if (scaled_height > self.screen_height):
                scaled_height = self.screen_height
                scaled_width = int((float(self.screen_height) / img_height) * img_width)
            else:
                scaled_width = self.screen_width

            img_bitsize = img.get_bitsize()

            # transform.smoothscale() can only be used for 24-bit and 32-bit images. If this is not a 24-bit or 32-bit
            # image, use transform.scale() instead which will be ugly but at least will work
            if (img_bitsize == 24 or img_bitsize == 32):
                img = pygame.transform.smoothscale(img, [scaled_width, scaled_height])
            else:
                img = pygame.transform.scale(img, [scaled_width, scaled_height])

            # Determine where to place the image so it will appear centered on the screen
            display_x = (self.screen_width - scaled_width) / 2
            display_y = (self.screen_height - scaled_height) / 2
        else:
            # No scaling was applied, so image is already full-screen
            display_x = 0
            display_y = 0

        # Blank screen before showing photo in case it doesn't fill the whole screen
        self.screen.fill([0, 0, 0])
        self.screen.blit(img, [display_x, display_y])
        pygame.display.update()

    def show_video(self, filename):
        # Videos will not be scaled - this needs to be done during transcoding
        # Blank screen before showing video in case it doesn't fill the whole screen
        self.screen.fill([0, 0, 0])
        pygame.display.update()
        subprocess.call(['/usr/bin/omxplayer', '-o', 'hdmi', filename], shell=False)
        # This might not be necessary, but it's there in case any stray copies of omxplayer.bin are somehow left running
        subprocess.call(['/usr/bin/killall', 'omxplayer.bin'], shell=False)

    def open_image(self, filename):
        """Returns a buffer representation and image orientation"""
        try:
            image = Image.open(filename)

            mode = image.mode
            size = image.size
            data = image.tobytes()
            orientation = self.get_orientation(image)

            image.close()
            return ((data, size, mode), orientation)

        except:
            return ((bytearray(3), (1, 1), "RGB"), 0) # A blank image

    def get_orientation(self, image : Image):
        try:
            for orientation in ExifTags.TAGS.keys():
              if ExifTags.TAGS[orientation] == 'Orientation':
                break
            exif = dict(image._getexif().items())

            # Convert to required clockwise rotation in deg, default 0
            return { 6: -90, 8: -270, 3: -180 }.get(exif[orientation], 0)

        except:
          return 0

main()

# TODO
#        # Play videos based on the specified probability. Check to be sure we have any videos
#        # to play before we try to play one.
#        if ((random.random() <= VIDEO_PROBABILITY) and (len(videos) > 0)):
#            player.show_video(videos[random.randint(0, len(videos) - 1)])

